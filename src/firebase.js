import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

const config = {
  apiKey: "AIzaSyCxsdqMf6LKcbL69MhyX2Pn0xUYQCFWIrs",
  authDomain: "projects-internal-kanzucode-co.firebaseapp.com",
  databaseURL: "https://projects-internal-kanzucode-co.firebaseio.com",
  projectId: "projects-internal-kanzucode-co",
  storageBucket: "projects-internal-kanzucode-co.appspot.com",
  messagingSenderId: "922144637302",
  appId: "1:922144637302:web:0be2e53a91c57f424afa57",
  measurementId: "G-MR5STVHDZJ"
};
firebase.initializeApp(config);
export default firebase;
