import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Login from "./components/Auth/Login";
import Projects from "./components/Projects";
import NotFound from "./components/NotFound";
import SimpleStorage from "react-simple-storage";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

class App extends Component {
  state = {
    authenticated: false,
    posts: [],
    updated: false,
    message: null
  };

  onLogin = () => {
    this.setState({ authenticated: true });
  };

  onLogout = e => {
    e.preventDefault();
    this.setState({ authenticated: false });
  };

  render() {
    return (
      <Router>
        <div className="App">
          <SimpleStorage parent={this} />
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Kanzu Code Projects
              {this.state.authenticated ? (
                <a
                  href="/logout"
                  className="App-header__logout"
                  id="logout"
                  onClick={this.onLogout}
                >
                  Logout
                </a>
              ) : (
                ""
              )}
            </p>
          </header>
          <Switch>
            <Route
              exact
              path="/"
              render={() =>
                !this.state.authenticated ? (
                  <Login onLogin={this.onLogin} />
                ) : (
                  <Redirect to="/projects" />
                )
              }
            />
            <Route
              exact
              path="/projects"
              render={() =>
                this.state.authenticated ? <Projects /> : <Redirect to="/" />
              }
            />
            <Route path="/projects" component={Projects} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
