import React, { Component } from "react";
import Cookies from "js-cookie";

export default class Login extends Component {
  state = {
    email: "",
    password: "",
    message: ""
  };

  setLoginMessage = message => {
    this.setState({ message });
  };

  handleLogin = e => {
    e.preventDefault();
    let url = "https://api.codeable.io/users/login"; //@TODO Central definition of URLs
    fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
        timezone_offset: -180
      })
    })
      .then(response => response.json())
      .then(loginResponse => {
        if (loginResponse.errors[0].message !== undefined) {
          this.setLoginMessage(loginResponse.errors[0].message);
        } else {
          Cookies.set("token", loginResponse.auth_token);
          this.props.onLogin();
        }
      })
      .catch(error => {
        this.setLoginMessage(
          "Sorry, an unexpected error occurred. Please retry"
        );
        console.error(error);
      });
  };
  render() {
    return (
      <div id="login">
        <form name="login" onSubmit={this.handleLogin}>
          <p>
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              onChange={e => this.setState({ email: e.target.value })}
            />
          </p>
          <p>
            <label htmlFor="password">Password:</label>
            <input
              type="password"
              onChange={e => this.setState({ password: e.target.value })}
            />
          </p>
          <p>
            <button
              type="submit"
              disabled={!this.state.email && !this.state.password}
            >
              Login
            </button>
            <span className="login-form__error-message">
              {this.state.message}
            </span>
          </p>
        </form>
      </div>
    );
  }
}
