import React, { Component } from "react";
//import firebase from "../firebase";
import Cookies from "js-cookie";

class Projects extends Component {
  state = {
    projects: []
  };

  componentDidMount() {
    let url =
      "https://api.codeable.io/users/me/tasks/in-progress?page=1&per_page=20";
    fetch(url, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: "Bearer " + Cookies.get("token")
      }
    })
      .then(response => response.json())
      .then(projects => {
        //const projectsRef = firebase.database().ref("projects");
        //projects.map(project => projectsRef.push(project)); // @TODO Save to firebase & save project due date
        this.setState({ projects });
      })
      .catch(error => console.error(error));
  }

  render() {
    return (
      <article className="projects">
        <h2>Ongoing Projects</h2>
        <div className="projects__inprogress">
          {this.state.projects.length < 1 && (
            <div className="card mb-3">
              <h3 className="card-header">No projects...</h3>
              <div className="card-body">
                <h5 className="card-title">Go forth and get more projects!</h5>
              </div>
            </div>
          )}
          {this.state.projects.length > 1 &&
            this.state.projects.map(project => (
              <div className="card mb-3" key={project.id}>
                <h3 className="card-header">
                  #{project.id} - {project.title}
                </h3>
                <div className="card-body">
                  <h5 className="card-title">Project Type: {project.kind}</h5>
                  <h6 className="card-subtitle text-muted">
                    {project.client.full_name}{" "}
                  </h6>
                  <small>Created: {project.published_at}</small>
                </div>
              </div>
            ))}
        </div>
      </article>
    );
  }
}

export default Projects;
